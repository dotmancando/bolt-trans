package main

import (
	"fmt"
	bolt "github.com/johnnadratowski/golang-neo4j-bolt-driver"
)

func main() {
	driver := bolt.NewDriver()
	conn, _ := driver.OpenNeo("bolt://localhost:7687")
	defer conn.Close()

	tx, err := conn.Begin()
	if err != nil {
		fmt.Println(err)
	}

	stmt, err := conn.PrepareNeo("CREATE (n:DotThing {id:\"1\"})")
	if err != nil {
		fmt.Println(err)
	}

	_, err = stmt.ExecNeo(nil)
	if err != nil {
		fmt.Println(err)
	}

	err = stmt.Close()
	if err != nil {
		fmt.Println(err)
	}

	for i := 0; i < 30; i++ {
		query := fmt.Sprintf("MATCH (n:DotThing {id:\"%d\"}) MERGE (n)-[:love]->(n2:DotThing {id:\"%d\"})", i, i+1)
		stmt, err := conn.PrepareNeo(query)
		if err != nil {
			fmt.Println(err)
		}

		_, err = stmt.ExecNeo(nil)
		if err != nil {
			fmt.Println(err)
		}

		err = stmt.Close()
		if err != nil {
			fmt.Println(err)
		}

	}

	err = tx.Commit()
	if err != nil {
		fmt.Println(err)
	}
}
